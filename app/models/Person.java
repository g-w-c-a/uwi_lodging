package models;

import java.util.ArrayList;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Person {
    public long id, trn;
    public String firstName, lastName;
    public Datetime dob;
    public User user;
    public Account account;
    public ArrayList<Post> posts;

    public Person(long id, long trn, String firstName, String lastName, Datetime dob, User user, Account account, ArrayList<Post> posts) {
        this.id = id;
        this.trn = trn;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.user = user;
        this.account = account;
        this.posts = posts;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTrn() {
        return trn;
    }

    public void setTrn(long trn) {
        this.trn = trn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Datetime getDob() {
        return dob;
    }

    public void setDob(Datetime dob) {
        this.dob = dob;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public ArrayList<Post> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<Post> posts) {
        this.posts = posts;
    }
}
