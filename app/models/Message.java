package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Message extends Post {
    public long receiverId;

    public Message(long postId, long userId, Integer rating, String header, String content, Timestamp timestamp, long receiverId) {
        super(postId, userId, rating, header, content, timestamp);
        this.receiverId = receiverId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }
}
