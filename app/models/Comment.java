package models;

import java.util.ArrayList;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Comment extends Message {
    public ArrayList<Message> replies;

    public Comment(long postId, long userId, Integer rating, String header, String content, Timestamp timestamp, long receiverId, ArrayList<Message> replies) {
        super(postId, userId, rating, header, content, timestamp, receiverId);
        this.replies = replies;
    }

    public ArrayList<Message> getReplies() {
        return replies;
    }

    public void setReplies(ArrayList<Message> replies) {
        this.replies = replies;
    }
}