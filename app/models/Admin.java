package models;

import java.util.ArrayList;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Admin extends Person {
    public Integer authorityLevel;

    public Admin(long id, long trn, String firstName, String lastName, Datetime dob, User user, Account account, ArrayList<Post> posts, Integer authorityLevel) {
        super(id, trn, firstName, lastName, dob, user, account, posts);
        this.authorityLevel = authorityLevel;
    }

    public Integer getAuthorityLevel() {
        return authorityLevel;
    }

    public void setAuthorityLevel(Integer authorityLevel) {
        this.authorityLevel = authorityLevel;
    }
}