package models;

import java.util.ArrayList;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Property {
    public Integer propertyId, landlordId;
    public Location location;
    public ArrayList<Image> images;

    public Property(Integer propertyId, Integer landlordId, Location location, ArrayList<Image> images) {
        this.propertyId = propertyId;
        this.landlordId = landlordId;
        this.location = location;
        this.images = images;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(Integer landlordId) {
        this.landlordId = landlordId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }
}