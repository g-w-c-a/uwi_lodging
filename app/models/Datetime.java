package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Datetime {
    public Integer month, dayOfMonth, year;

    public Datetime(Integer month, Integer dayOfMonth, Integer year) {
        this.month = month;
        this.dayOfMonth = dayOfMonth;
        this.year = year;
    }

    public Datetime() {
        // Date hardcoded, use a function to replace this
        this.month = 10;
        this.dayOfMonth = 24;
        this.year = 2014;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(Integer dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return month + "/" + dayOfMonth + "/" + year;
    }
}