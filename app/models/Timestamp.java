package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Timestamp extends Datetime {
    public long timestampId, propertyId;
    public Integer hours, minutes, seconds;
    public boolean isEdited;

    public Timestamp(Integer month, Integer dayOfMonth, Integer year, long timestampId, long propertyId, Integer hours, Integer minutes, Integer seconds, boolean isEdited) {
        super(month, dayOfMonth, year);
        this.timestampId = timestampId;
        this.propertyId = propertyId;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.isEdited = isEdited;
    }

    public Timestamp(long timestampId, long propertyId, Integer hours, Integer minutes, Integer seconds, boolean isEdited) {
        this.timestampId = timestampId;
        this.propertyId = propertyId;
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
        this.isEdited = isEdited;
    }

    public long getTimestampId() {
        return timestampId;
    }

    public void setTimestampId(long timestampId) {
        this.timestampId = timestampId;
    }

    public long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(long propertyId) {
        this.propertyId = propertyId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    public boolean isEdited() {
        return isEdited;
    }

    public void setEdited(boolean isEdited) {
        this.isEdited = isEdited;
    }

    @Override
    public String toString() {
        String stamp = hours + ":" + minutes;

        if(isEdited)
            return stamp + "...has been edited";
        else
            return stamp;
    }
}
