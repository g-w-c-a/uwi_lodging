package models;

import java.util.ArrayList;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Landlord extends Person {
    public long landlordId;
    public ArrayList<Property> properties;

    public Landlord(long id, long trn, String firstName, String lastName, Datetime dob, User user, Account account, ArrayList<Post> posts, long landlordId, ArrayList<Property> properties) {
        super(id, trn, firstName, lastName, dob, user, account, posts);
        this.landlordId = landlordId;
        this.properties = properties;
    }

    public long getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(long landlordId) {
        this.landlordId = landlordId;
    }

    public ArrayList<Property> getProperties() {
        return properties;
    }

    public void setProperties(ArrayList<Property> properties) {
        this.properties = properties;
    }
}