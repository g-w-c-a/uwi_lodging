package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Review extends Post {
    public Review(long postId, long userId, Integer rating, String header, String content, Timestamp timestamp) {
        super(postId, userId, rating, header, content, timestamp);
    }
}
