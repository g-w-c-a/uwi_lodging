package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Post {
    public long postId, userId;
    public Integer rating;
    public String header, content;
    public Timestamp timestamp;

    public Post(long postId, long userId, Integer rating, String header, String content, Timestamp timestamp) {

        this.postId = postId;
        this.userId = userId;
        this.rating = rating;
        this.header = header;
        this.content = content;
        this.timestamp = timestamp;
    }

    public Post() {
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
