package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class User {
    public long sessionId;
    public String username, password;
    public Integer attempts, timeout;

    public User(long sessionId, String username, String password, Integer attempts, Integer timeout) {
        this.sessionId = sessionId;
        this.username = username;
        this.password = password;
        this.attempts = attempts;
        this.timeout = timeout;
    }

    public User(long sessionId, String username, String password, Integer timeout) {
        this.sessionId = sessionId;
        this.username = username;
        this.password = password;
        this.timeout = timeout;
        this.attempts = 5;
    }

    public long getSessionId() {
        return sessionId;
    }

    public void setSessionId(long sessionId) {
        this.sessionId = sessionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAttempts() {
        return attempts;
    }

    public void setAttempts(Integer attempts) {
        this.attempts = attempts;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
