package models;

import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Image {
    // TODO
    //Fix this up to actuallly load image
    public long imageId, propertyId;
    public BufferedImage image;
    public File file;
    public String imageLocation, description;
    public Timestamp timestamp;

    public Image(long imageId, long propertyId, BufferedImage image, File file, String imageLocation, String description, Timestamp timestamp) {
        this.imageId = imageId;
        this.propertyId = propertyId;
        this.image = image;
        this.file = file;
        this.imageLocation = imageLocation;
        this.description = description;
        this.timestamp = timestamp;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    public long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(long propertyId) {
        this.propertyId = propertyId;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
