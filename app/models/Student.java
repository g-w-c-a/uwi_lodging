package models;

import java.util.ArrayList;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Student extends Person {
    public Student(long id, long trn, String firstName, String lastName, Datetime dob, User user, Account account, ArrayList<Post> posts) {
        super(id, trn, firstName, lastName, dob, user, account, posts);
    }
}
