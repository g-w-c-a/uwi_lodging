package models;

/**
 * Created by Don Shane on 10/23/2014.
 */
public class Account {
    public long transactionId, transNo, senderId, receiverId;
    public double amount;

    public Account(long transactionId, long transNo, long senderId, long receiverId, double amount) {
        this.transactionId = transactionId;
        this.transNo = transNo;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.amount = amount;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public long getTransNo() {
        return transNo;
    }

    public void setTransNo(long transNo) {
        this.transNo = transNo;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean validateLogin(){
        return true;
    }
}
